# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.7.7] - 2020-1-14
- Updated To Commit Number 4623dc89e5af9e876a9313ea70f7644e046e2c40 - 2020-1-20
- Now We Support Spaces in Product name
- Now we can have different analytics keys in RasConfig for Bazaar, MyKet, Google and TV
- Managed and centralised Symbols list.
- Builds config now have AdSystem and handles Plugins Folder(although it's not a good idea to depend eyePM to ads systems).
- Simple animation clean uP.


## [1.6.3] - 2020-12-07
- Updated To Commit Number e63827ded7abe31b15374a58fd75a7a4398393fc - 2020-12-29


### TODO
- UI Tween Packs and Elements (@jaamySk)
- TV Buttons (@atiehZ)
