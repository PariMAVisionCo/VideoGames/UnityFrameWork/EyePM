﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;

namespace EyePM.Builder
{
    [CreateAssetMenu(menuName = "EyePM/Build Config")]
    public class BuildConfig : BuildConfigBase
    {
        public bool buildAppBundle = false;
        public string productName = string.Empty;
        public Market market = Market.Bazaar;
        public AdsSystem adsSystem = AdsSystem.None;
        public Architecture architecture = Architecture.ArmV7;
        [Space]
        public List<SceneAsset> scenes = new List<SceneAsset>();
        public string extraSymbols = string.Empty;
        public string removeSymbols = string.Empty;
        [Space]
        public List<FileInfo> files = new List<FileInfo>();

        public string FileName
        {
            get
            {
                var baseFilename = Application.identifier.Split(new char[] { '.' }, System.StringSplitOptions.RemoveEmptyEntries);
                var fileName = baseFilename.Length > 0 ? baseFilename.Last() : Application.productName.Replace(" ", string.Empty).ToLower();
                fileName += "_" + PlayerSettings.bundleVersion + "_" + market.ToString().ToLower() + "_";
                switch (architecture)
                {
                    case Architecture.ArmV7: fileName += "v7"; break;
                    case Architecture.Arm64: fileName += "v8"; break;
                    case Architecture.ArmV7_Arm64: fileName += "v7v8"; break;
                }
                fileName += (buildAppBundle ? ".aab" : ".apk");
                return fileName;
            }
        }

        public bool ValidateFiles()
        {
            var isValid = true;
            foreach (var item in files)
            {
                switch (item.type)
                {
                    case FileType.DisableFile: item.title = (item.destFile == null) ? "WARNING: No file!" : "Disable : " + item.destFile.name; break;
                    case FileType.ReplaceText: item.title = item.destFile == null ? "WARNING: No file!" : item.destFile.name + " : " + item.what + " > " + item.with.Replace("\n", string.Empty); break;
                    case FileType.ReplaceFile:
                    {
                        item.title = item.destFile == null ? "WARNING: No file!" : "Replace File : " + (item.sourceFile == null ? "WARNING: No source file!" : item.destFile.name + " > " + item.sourceFile.name);

                        if (item.sourceFile != null)
                            item.SourceName = AssetDatabase.GetAssetPath(item.sourceFile);
                        else
                            isValid = false;
                    }
                        break;
                }


                if (item.destFile != null)
                    item.Filename = AssetDatabase.GetAssetPath(item.destFile);
                else
                    isValid = false;
            }

            return isValid;
        }

        public bool BackupFiles()
        {
            foreach (var item in files)
            {
                if (item.type == FileType.DisableFile)
                {
                    if (BuilderFileHandler.DisableFile(item.Filename) == false)
                        return false;
                }
                else
                {
                    if (BuilderFileHandler.BackupFile(item.Filename) == false)
                        return false;
                }
            }
            return true;
        }

        public bool PerformReplaces()
        {
            foreach (var item in files)
            {
                switch (item.type)
                {
                    case FileType.ReplaceText:
                        if (BuilderFileHandler.ReplaceFileText(item.Filename, item.what, item.with) == false)
                            return false;
                        break;
                    case FileType.ReplaceFile:
                        if (BuilderFileHandler.ReplaceWholeFile(item.SourceName, item.Filename) == false)
                            return false;
                        break;
                    case FileType.DisableFile: break;
                }
            }
            return true;
        }

        private void OnValidate()
        {
            ValidateFiles();
        }
    }
}
#endif
