﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;


namespace EyePM.Builder
{
    [CustomEditor(typeof(Builder))]
    public class BuilderEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var builder = target as Builder;
            if (builder == null)
                return;

            // verify files
            if (isBuilding == false && ValidateFiles(builder) == false)
                EditorGUILayout.HelpBox("File reference(s) missing! Please check file references!", MessageType.Warning);
            else
            {
                PlayerSettings.bundleVersion = builder.version + "." + builder.bundleVersionCode;

                // prepare build configs
                for (int i = 0; i < builder.builds.Count; i++)
                {
                    var build = builder.builds[i];
                    var spaces = build.config.market switch
                    {
                        Market.TV => "               ",
                        Market.Bazaar => "         ",
                        Market.Myket => "          ",
                        _ => "   "
                    };
                    build.title = (build.activated ? "✓ " : "X  ") + i + ":" + build.config.market + spaces + " |   " + (builder.isReleaseCandidate ? "RC_" : "") + build.config.FileName;
                }

                if (GUILayout.Button("Let's Build"))
                {
                    StartBuilds(builder);
                }

                if (GUILayout.Button("Apply In Editor"))
                {
                    ApplyInEditor(builder, builder.applyInEditorIndex);
                }
            }

            base.OnInspectorGUI();
        }

        //////////////////////////////////////////////////////
        /// STATIC MEMBERS
        //////////////////////////////////////////////////////
        private static bool isBuilding = false;
        private static bool buildResult = true;
        private static string initialSymbols = string.Empty;
        private static string initialProductName = string.Empty;
        private static AndroidArchitecture initialArchitectures = AndroidArchitecture.All;

        private static bool ValidateFiles(Builder builder)
        {
            foreach (var build in builder.builds)
            {
                if (build.config == null || build.config.ValidateFiles() == false)
                    return false;
            }
            return true;
        }

        private static async Task WaitForEditor()
        {
            await Task.Delay(100);
            AssetDatabase.Refresh();
            await Task.Delay(100);
            while (EditorApplication.isUpdating)
                await Task.Delay(100);
        }

        private void ApplyInEditor(Builder builder, int toApplyConfigIndex)
        {
            BuildConfig config = builder.builds[toApplyConfigIndex].config;
            HandleAndroidPluginFolder(config.adsSystem);
            HandleSymbols(SymbolsList.GetCurrentSymbols(), config, builder.editorDebugMode);

            if (config.scenes.Count == 0)
                return;
            EditorSettings.prefabRegularEnvironment = config.scenes[0];
            EditorSettings.prefabUIEnvironment = config.scenes[0];
        }

        private static async void StartBuilds(Builder builder)
        {
            if (isBuilding || ValidateFiles(builder) == false)
                return;

            var path = Directory.GetParent(Application.dataPath).Parent.FullName + "Builds\\";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            var folder = EditorUtility.SaveFolderPanel("Select Destination Folder", path, null);
            if (string.IsNullOrEmpty(folder))
                return;
            isBuilding = true;
            EditorApplication.LockReloadAssemblies();

            initialProductName = PlayerSettings.productName;
            initialArchitectures = PlayerSettings.Android.targetArchitectures;
            initialSymbols = SymbolsList.GetCurrentSymbols();
            try
            {

                //var hadExcludesFolder = SymbolicLinker.ExistExcludes;
                //if (hadExcludesFolder) SymbolicLinker.RemoveExcludes();
                PlayerSettings.bundleVersion = builder.version + "." + builder.bundleVersionCode;
                PlayerSettings.Android.bundleVersionCode = builder.bundleVersionCode;
                PlayerSettings.Android.keystorePass = builder.keystore.storePassword;
                PlayerSettings.Android.keyaliasPass = builder.keystore.aliasesPassword;

                buildResult = true;
                for (int index = 0; index < builder.builds.Count; index++)
                {
                    Builder.currentBuilding = builder.builds[index];
                    if (Builder.currentBuilding.activated == false) continue;

                    Debug.Log("Build " + (index + 1) + " of " + builder.builds.Count + " started...");
                    await Task.Delay(10);
                    await BuildOneConfig(Builder.currentBuilding.config, Path.Combine(folder, Builder.currentBuilding.config.FileName), index == builder.buildAndRunIndex, builder.buildDebugMode);
                    await Task.Delay(10);
                    Debug.Log("Build " + (index + 1) + " of " + builder.builds.Count + " finished.");

                    if (builder.stopQueueOnError && buildResult == false)
                        break;
                    else
                    {
                        await WaitForEditor();
                        SymbolsList.SetProjectSymbols(initialSymbols);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError("Build Error " + e);
            }

            Debug.Log("Done. Finished building process.");
            ResetToPreBuildState();
        }

        private static void ResetToPreBuildState()
        {
            //if (hadExcludesFolder) SymbolicLinker.AddExcludes();
            SymbolsList.SetProjectSymbols(initialSymbols);
            isBuilding = false;
            EditorApplication.UnlockReloadAssemblies();
        }

        private static async Task BuildOneConfig(BuildConfig config, string path, bool autoRun, bool editMode)
        {
            PlayerSettings.productName = config.productName.HasContent(3) ? config.productName : initialProductName;
            PlayerSettings.Android.targetArchitectures = (AndroidArchitecture) config.architecture;
            EditorBuildSettings.scenes = (from scene in config.scenes select AssetDatabase.GetAssetPath(scene) into scenePath where !string.IsNullOrEmpty(scenePath) select new EditorBuildSettingsScene(scenePath, true)).ToArray();

            EditorUserBuildSettings.buildAppBundle = config.buildAppBundle;
            PlayerSettings.Android.androidTVCompatibility = config.market == Market.TV;

            HandleAndroidPluginFolder(config.adsSystem);
            HandleSymbols(initialSymbols, config, editMode);
            await WaitForEditor();

            if (config.BackupFiles() && config.PerformReplaces())
            {
                await WaitForEditor();
                buildResult = Build(path, autoRun);
                await WaitForEditor();
            }
            BuilderFileHandler.RestoreAllFiles();
            await WaitForEditor();

            EditorUserBuildSettings.buildAppBundle = false;
            PlayerSettings.productName = initialProductName;
            PlayerSettings.Android.targetArchitectures = initialArchitectures;
            SymbolsList.SetProjectSymbols(initialSymbols);
            await WaitForEditor();
        }

        private static void HandleAndroidPluginFolder(AdsSystem adsSystem)
        {
            if (adsSystem == AdsSystem.None)
                return;

            string source = Application.dataPath + "/Plugins/Android";
            string dest = Application.dataPath + "/Plugins/Android_" + adsSystem;
            FileUtil.DeleteFileOrDirectory(source);

            try
            {
                FileUtil.CopyFileOrDirectory(dest, source);
            }
            catch (Exception e)
            {
                Debug.LogError($"WTF! Folder {dest} not found!. Maybe the Package of {adsSystem} is not imported, or you have changed It's name! Unity Detail {e}");
                return;
            }

            DirectoryInfo dir = new DirectoryInfo(source);
            System.IO.FileInfo[] info = dir.GetFiles("*.meta");
            foreach (System.IO.FileInfo fileInfo in info)
                FileUtil.DeleteFileOrDirectory(fileInfo.FullName);
            AssetDatabase.Refresh();
        }

        private static void HandleSymbols(string initial, BuildConfig config, bool debugMode)
        {
            var symbols = initial;

            symbols = HandleMarketSymbols(symbols, config.market);
            symbols = HandleAdsSystemSymbols(symbols, config.adsSystem);
            symbols = HandleOtherSymbols(symbols, debugMode, config.extraSymbols, config.removeSymbols);

            SymbolsList.SetProjectSymbols(symbols);
        }

        private static string HandleMarketSymbols(string symbols, Market market)
        {
            symbols = SymbolsList.AddRemoveSymbol(
                                                  symbols,
                                                  SymbolsList.BazaarBuild + ";" + SymbolsList.MyKetBuild + ";" + SymbolsList.GoogleBuild + ";" + SymbolsList.TvBuild + ";",
                                                  false
                                                 );
            switch (market)
            {
                case Market.Bazaar:
                    symbols = SymbolsList.AddRemoveSymbol(symbols, SymbolsList.BazaarBuild, true);
                    break;
                case Market.Myket:
                    symbols = SymbolsList.AddRemoveSymbol(symbols, SymbolsList.MyKetBuild, true);
                    break;
                case Market.GooglePlay:
                    symbols = SymbolsList.AddRemoveSymbol(symbols, SymbolsList.GoogleBuild, true);
                    break;
                case Market.TV:
                    symbols = SymbolsList.AddRemoveSymbol(symbols, SymbolsList.TvBuild, true);
                    break;
                default: throw new ArgumentOutOfRangeException();
            }
            return symbols;
        }

        private static string HandleAdsSystemSymbols(string symbols, AdsSystem adsSystem)
        {
            symbols = SymbolsList.AddRemoveSymbol(
                                                  symbols,
                                                  SymbolsList.AdMob + ";" + SymbolsList.UAD + ";" + SymbolsList.Tapsell + ";" + SymbolsList.TapsellPlus + ";",
                                                  false
                                                 );
            switch (adsSystem)
            {
                case AdsSystem.None:
                    break;
                case AdsSystem.AdMob:
                    symbols = SymbolsList.AddRemoveSymbol(symbols, SymbolsList.AdMob, true);
                    break;
                case AdsSystem.UnityAds:
                    symbols = SymbolsList.AddRemoveSymbol(symbols, SymbolsList.UAD, true);
                    break;
                case AdsSystem.Tapsell:
                    symbols = SymbolsList.AddRemoveSymbol(symbols, SymbolsList.Tapsell, true);
                    break;
                case AdsSystem.TapsellPlus:
                    symbols = SymbolsList.AddRemoveSymbol(symbols, SymbolsList.TapsellPlus, true);
                    break;
                default: throw new ArgumentOutOfRangeException(nameof(adsSystem), adsSystem, null);
            }
            return symbols;
        }

        private static string HandleOtherSymbols(string symbols, bool debugMode, string configExtraSymbols, string configRemoveSymbols)
        {
            symbols = SymbolsList.AddRemoveSymbol(symbols, SymbolsList.DebugMode + ";" + SymbolsList.ConsoleCommands, debugMode);
            symbols = SymbolsList.AddRemoveSymbol(symbols, configExtraSymbols, true);
            symbols = SymbolsList.AddRemoveSymbol(symbols, configRemoveSymbols, false);
            return symbols;
        }

        private static bool Build(string locationPathName, bool autoRun)
        {
            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            buildPlayerOptions.scenes = EditorBuildSettings.scenes.Select(x => x.path).ToArray();
            buildPlayerOptions.target = BuildTarget.Android;
            buildPlayerOptions.options = BuildOptions.CompressWithLz4HC | BuildOptions.ShowBuiltPlayer;
            buildPlayerOptions.locationPathName = locationPathName;

            if (autoRun)
                buildPlayerOptions.options |= BuildOptions.AutoRunPlayer;

            try
            {
                BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
                BuildSummary summary = report.summary;

                switch (summary.result)
                {
                    case BuildResult.Succeeded:
                        Debug.Log("Don't forget my shirini, Build succeeded in " + (int) summary.totalTime.TotalSeconds + " seconds with " + summary.totalSize + " bytes");
                        return true;
                    case BuildResult.Failed:
                        Debug.Log("Shit, Shit, Shit, Build failed!");
                        return false;
                    case BuildResult.Cancelled:
                        Debug.Log("Are you Moody? Build cancelled!");
                        return false;
                    default:
                        Debug.Log("What the hell? Build result is unknown!");
                        return false;
                }
            }
            catch (Exception e)
            {
                Debug.Log("Shit, Shit, Shit, Build failed: " + e.Message);
                return false;
            }
        }
    }
}