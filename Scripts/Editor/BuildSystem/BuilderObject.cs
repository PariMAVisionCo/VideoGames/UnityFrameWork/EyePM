﻿#if UNITY_EDITOR
using UnityEngine;


namespace EyePM.Builder
{
    public enum Market
    {
        Bazaar = 1,
        Myket = 2,
        GooglePlay = 3,
        TV = 4
    }

    public enum AdsSystem
    {
        None,
        AdMob,
        UnityAds,
        Tapsell,
        TapsellPlus
    }

    public enum Architecture
    {
        ArmV7 = 1,
        Arm64 = 2,
        ArmV7_Arm64 = 3
    }

    [System.Serializable]
    public class Keystore
    {
        public string storePassword = string.Empty;
        public string aliasesPassword = string.Empty;
    }

    public class BuildConfigBase : ScriptableObject
    {
        public virtual void PreBuild() { }
        public virtual void PostBuild() { }
    }
}
#endif
