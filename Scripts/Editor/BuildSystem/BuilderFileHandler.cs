﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;


namespace EyePM.Builder
{
    public static class BuilderFileHandler
    {
        private class BackupFiles
        {
            public string filename = string.Empty;
            public string backPath = string.Empty;
        }

        private const string META = ".meta";

        private static readonly List<BackupFiles> BackupedFiles = new List<BackupFiles>();

        private static string AssetsFolder => Path.GetFullPath(Application.dataPath + Path.DirectorySeparatorChar);
        private static string BackFolder => Path.GetFullPath(Path.Combine(AssetsFolder, "../BuilderBackup") + Path.DirectorySeparatorChar);
        public static string ProjectFolder => Path.GetFullPath(Path.Combine(AssetsFolder, "../"));

        private static string MakePath(string str1, string str2)
        {
            return Path.GetFullPath(Path.Combine(str1, str2));
        }

        private static string RelativePath(string fullpath, string relative)
        {
            return fullpath.Replace(relative, string.Empty);
        }

        private static string PathToBackup(string filename)
        {
            return MakePath(BackFolder, RelativePath(filename, AssetsFolder));
        }

        private static string PathToAssets(string filename)
        {
            return MakePath(AssetsFolder, RelativePath(filename, BackFolder));
        }

        private static void ValidateDirectory(string filename)
        {
            var dir = Path.GetDirectoryName(filename);
            if (Directory.Exists(dir) == false)
                Directory.CreateDirectory(dir ?? string.Empty);
        }

        private static bool MoveFile(string srcFilename, string destFilename, bool deleteSourceFile)
        {
            ValidateDirectory(destFilename);
            try
            {
                File.Copy(srcFilename, destFilename, true);
                File.Copy(srcFilename + META, destFilename + META, true);
                if (deleteSourceFile) DeleteFile(srcFilename);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                return false;
            }
            return true;
        }

        private static bool DeleteFile(string filename)
        {
            try
            {
                if (File.Exists(filename))
                    File.Delete(filename);
                if (File.Exists(filename + META))
                    File.Delete(filename + META);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                return false;
            }
            return true;
        }

        public static bool BackupFile(string fileName)
        {
            fileName = Path.GetFullPath(fileName);
            if (File.Exists(fileName) == false) return false;
            if (BackupedFiles.Exists(x => x.filename.Equals(fileName, StringComparison.OrdinalIgnoreCase))) return true;

            var backup = new BackupFiles
                         {
                             filename = PathToAssets(fileName),
                             backPath = PathToBackup(fileName)
                         };

            if (MoveFile(backup.filename, backup.backPath, false))
            {
                BackupedFiles.Add(backup);
                return true;
            }
            return false;
        }

        public static bool DisableFile(string filename)
        {
            filename = Path.GetFullPath(filename);
            if (File.Exists(filename) == false) return true;
            if (BackupedFiles.Exists(x => x.filename.Equals(filename, StringComparison.OrdinalIgnoreCase))) return true;

            var backup = new BackupFiles
                         {
                             filename = PathToAssets(filename),
                             backPath = PathToBackup(filename)
                         };

            if (MoveFile(backup.filename, backup.backPath, true))
            {
                BackupedFiles.Add(backup);
                return true;
            }
            return false;
        }

        public static bool ReplaceFileText(string filename, string what, string with)
        {
            if (File.Exists(filename) == false) return false;
            try
            {
                var text = File.ReadAllText(filename);
                text = text.Replace(what, with);
                File.WriteAllText(filename, text);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                return false;
            }
            return true;
        }

        public static bool ReplaceWholeFile(string srcFilename, string destFileName)
        {
            if (File.Exists(srcFilename) == false) return false;
            try
            {
                ValidateDirectory(destFileName);
                File.WriteAllBytes(destFileName, File.ReadAllBytes(srcFilename));
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                return false;
            }
            return true;
        }

        public static bool RestoreFile(string fileName)
        {
            fileName = Path.GetFullPath(fileName);
            var backup = BackupedFiles.Find(x => x.filename == fileName);
            if (backup == null) return true;
            if (MoveFile(backup.filename, backup.backPath, false))
            {
                BackupedFiles.Remove(backup);
                return true;
            }
            return false;
        }

        public static bool RestoreAllFiles()
        {
            bool noError = true;

            // restore all files
            foreach (var item in BackupedFiles)
            {
                if (MoveFile(item.backPath, item.filename, false) == false)
                    noError = false;
            }

            // remove all files from backup place
            if (noError)
            {
                foreach (var item in BackupedFiles)
                {
                    DeleteFile(item.backPath);
                }

                BackupedFiles.Clear();
                try
                {
                    if (Directory.Exists(BackFolder))
                        Directory.Delete(BackFolder, true);
                }
                catch (Exception e)
                {
                    Debug.LogError("Build Semi Major Error, Backup directory could not be deleted. " + e);
                }
            }

            return noError;
        }
    }
}
#endif