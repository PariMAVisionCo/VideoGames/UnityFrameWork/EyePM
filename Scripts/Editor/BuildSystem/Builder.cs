﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;


namespace EyePM.Builder
{
    [CreateAssetMenu(menuName = "EyePM/Build Settings")]
    public class Builder : ScriptableObject
    {
        [Serializable]
        public class Build
        {
            [HideInInspector] public string title = string.Empty;
            public bool activated = false;
            public BuildConfig config = null;
        }

        [Header("Options:")]
        public bool isReleaseCandidate;
        public bool editorDebugMode;
        public bool buildDebugMode;
        public int applyInEditorIndex = -1;
        public int buildAndRunIndex = -1;
        public bool stopQueueOnError = true;

        [Header("Build Configurations:"), Space]
        public int version = 1;
        public int bundleVersionCode = 1;
        [Space]
        public Keystore keystore = new Keystore();
        [Space, Space]
        public List<Build> builds = new List<Build>();
        //////////////////////////////////////////////////////
        /// STATIC MEMBERS
        //////////////////////////////////////////////////////
        public static Build currentBuilding = null;

        private static Builder instance = default;

        public static Builder Instance
        {
            get
            {
                if (instance == null)
                    CreateMe(CreateInstance<Builder>(), nameof(Builder));
                if (instance == null)
                    instance = Resources.Load<Builder>("BaseConfigs/" + nameof(Builder));
                return instance;
            }
        }

        [MenuItem("EyePM/Build", priority = 50)]
        private static void SelectMe()
        {
            Selection.activeObject = Instance;
        }

        private static void CreateMe(Object instance, string name)
        {
            var path = "/_" + Application.productName.Replace(" ", string.Empty) + "/Resources/BaseConfigs/";
            var appPath = Application.dataPath + path;
            var fileName = path + name + ".asset";
            if (System.IO.File.Exists(Application.dataPath + fileName)) return;
            if (System.IO.Directory.Exists(appPath) == false) System.IO.Directory.CreateDirectory(appPath);
            AssetDatabase.CreateAsset(instance, "Assets" + fileName);
            AssetDatabase.SaveAssets();
        }
    }
}
#endif