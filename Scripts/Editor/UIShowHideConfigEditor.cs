﻿using UnityEngine;
using UnityEditor;


namespace EyePM
{
    [CustomEditor(typeof(UIShowHideConfig))]
    public class UIShowHideConfigEditor : Editor
    {
        private UIShowHideConfig config = null;

        private int NextId
        {
            get
            {
                int res = 0;
                foreach (var item in config.configs)
                    if (item.id > res)
                        res = item.id;
                return res + 1;
            }
        }

        private void OnEnable()
        {
            config = target as UIShowHideConfig;
        }

        public override void OnInspectorGUI()
        {

            if (GUILayout.Button("Add Config"))
            {
                config.configs.Add(new UIShowHideConfig.State()
                                   {
                                       name = "Element " + NextId,
                                       id = NextId
                                   });
            }
            EditorGUILayout.Space();

            serializedObject.Update();
            var iterator = serializedObject.FindProperty("configs");


            foreach (SerializedProperty item in iterator)
            {
                var rect = EditorGUILayout.GetControlRect(true, 25);
                EditorGUI.HelpBox(rect, string.Empty, MessageType.None);

                if (GUI.Button(new Rect(rect.x + rect.width - 25, rect.y, 25, 25), "X"))
                {
                    item.DeleteCommand();
                }

                item.isExpanded = EditorGUI.Foldout(rect, item.isExpanded, " " + item.displayName, true);
                if (item.isExpanded)
                {
                    EditorGUI.indentLevel++;
                    item.NextVisible(true);
                    EditorGUILayout.PropertyField(item);
                    item.NextVisible(true);
                    EditorGUILayout.PropertyField(item);
                    item.NextVisible(false);
                    EditorGUILayout.PropertyField(item);
                    item.NextVisible(false);
                    EditorGUILayout.PropertyField(item);
                    item.NextVisible(false);
                    EditorGUILayout.PropertyField(item);
                    item.NextVisible(false);
                    EditorGUILayout.PropertyField(item);
                    item.NextVisible(false);
                    EditorGUILayout.PropertyField(item);
                    EditorGUI.indentLevel--;
                }
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
}
