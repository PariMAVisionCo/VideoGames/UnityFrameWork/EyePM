﻿using UnityEditor;
using UnityEngine;


namespace EyePM
{
    [CustomEditor(typeof(UIResourceParticleConfig))]
    public class UIResourceParticleConfigEditor : Editor
    {
        private UIResourceParticleConfig config = null;

        private int NextId
        {
            get
            {
                int res = 0;
                foreach (var item in config.configs)
                    if (item.id > res)
                        res = item.id;
                return res + 1;
            }
        }

        private void OnEnable()
        {
            config = target as UIResourceParticleConfig;
        }

        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Add Config"))
            {
                config.configs.Add(new UIResourceParticleConfig.Data()
                                   {
                                       name = "Element " + NextId,
                                       id = NextId
                                   });
            }
            EditorGUILayout.Space();

            serializedObject.Update();
            var iterator = serializedObject.FindProperty("configs");


            foreach (SerializedProperty item in iterator)
            {
                var rect = EditorGUILayout.GetControlRect(true, 25);
                EditorGUI.HelpBox(rect, string.Empty, MessageType.None);

                if (GUI.Button(new Rect(rect.x + rect.width - 25, rect.y, 25, 25), "X"))
                {
                    item.DeleteCommand();
                }

                item.isExpanded = EditorGUI.Foldout(rect, item.isExpanded, " " + item.displayName, true);
                if (item.isExpanded)
                {
                    EditorGUI.indentLevel++;
                    item.NextVisible(true);
                    EditorGUILayout.PropertyField(item);
                    EditorGUILayout.Space(5);
                    item.NextVisible(false);
                    EditorGUILayout.PropertyField(item);
                    item.NextVisible(false);
                    EditorGUILayout.PropertyField(item);
                    item.NextVisible(false);
                    EditorGUILayout.PropertyField(item);
                    item.NextVisible(false);
                    EditorGUILayout.PropertyField(item);
                    item.NextVisible(false);
                    EditorGUILayout.PropertyField(item);
                    item.NextVisible(false);
                    EditorGUILayout.PropertyField(item);
                    item.NextVisible(false);
                    EditorGUILayout.PropertyField(item);

                    EditorGUILayout.Space(5);
                    EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
                    EditorGUILayout.Space(10);

                    EditorGUI.indentLevel--;
                }
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
}
