﻿using UnityEditor;
using UnityEngine;


namespace EyePM
{
    public class UIResourceParticleManagerEditor<T> : Editor where T : MonoBehaviour, IUIResourceManagerBase
    {
        private Transform source = null;
        private int amount = 0;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Space(10);
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            var style = GUI.skin.GetStyle("Label");
            style.alignment = TextAnchor.UpperCenter;
            style.fontStyle = FontStyle.Bold;
            EditorGUILayout.LabelField("IN EDITOR TEST", style);
            EditorGUILayout.Space(5);

            style.fontStyle = FontStyle.Normal;
            if (!source)
                EditorGUILayout.LabelField("Assign Source", style);
            else if (amount == 0)
                EditorGUILayout.LabelField("Add Amount", style);

            source = EditorGUILayout.ObjectField("Source", source, typeof(Transform), true) as Transform;

            if (source)
            {
                amount = EditorGUILayout.IntField("Amount", amount);
                if (amount != 0)
                {
                    EditorGUILayout.Space(5);
                    T manager = target as T;
                    if (manager != null && GUILayout.Button("Play"))
                    {
                        if (source as RectTransform)
                            manager.Add(100, source as RectTransform);
                        else
                        {
                            var canvas = manager.transform.GetComponentInParent<Canvas>();
                            if (canvas == null)
                            {
                                EditorGUILayout.HelpBox("Resource Particle Manager Component should be on a gameObject which is child of a canvas. canvas not found!", MessageType.Error);
                                return;
                            }
                            manager.Add(amount, source, canvas);
                        }
                    }
                }
            }

            EditorGUILayout.Space(15);
        }
    }
}
