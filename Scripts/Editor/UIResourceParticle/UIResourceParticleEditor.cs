﻿using System.Collections.Generic;
using UnityEditor;


namespace EyePM
{
    [CustomEditor(typeof(UIResourceParticle))]
    public class UIResourceParticleEditor : Editor
    {
        private readonly List<string> popupLabels = new List<string>();
        private readonly List<int> popupIds = new List<int>();

        private void OnEnable()
        {
            popupLabels.Add("None");
            popupIds.Add(0);
            foreach (var item in UIResourceParticleConfig.Instance.configs)
            {
                popupLabels.Add(item.name);
                popupIds.Add(item.id);
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            var configId = serializedObject.FindProperty(nameof(UIResourceParticle.configId));
            var config = serializedObject.FindProperty(nameof(UIResourceParticle.config));

            InspectorConfigPopup("Reference", configId, config);

            if (configId.intValue == 0)
                InspectorConfig(config);

            serializedObject.ApplyModifiedProperties();
        }

        private void InspectorConfig(SerializedProperty config)
        {
            var rect = EditorGUILayout.GetControlRect(true, 25);
            EditorGUI.HelpBox(rect, string.Empty, MessageType.None);
            config.isExpanded = EditorGUI.Foldout(rect, config.isExpanded, " " + config.displayName, true);
            if (config.isExpanded)
            {
                EditorGUI.indentLevel++;
                config.NextVisible(true);
                EditorGUILayout.PropertyField(config);
                EditorGUILayout.Space(5);
                config.NextVisible(false);
                EditorGUILayout.PropertyField(config);
                config.NextVisible(false);
                EditorGUILayout.PropertyField(config);
                config.NextVisible(false);
                EditorGUILayout.PropertyField(config);
                config.NextVisible(false);
                EditorGUILayout.PropertyField(config);
                config.NextVisible(false);
                EditorGUILayout.PropertyField(config);
                config.NextVisible(false);
                EditorGUILayout.PropertyField(config);
                config.NextVisible(false);
                EditorGUI.indentLevel--;
            }
        }

        private void InspectorConfigPopup(string label, SerializedProperty configId, SerializedProperty currentData)
        {
            var newValue = EditorGUILayout.IntPopup(label, configId.hasMultipleDifferentValues ? -1 : configId.intValue, popupLabels.ToArray(), popupIds.ToArray());
            if (newValue == -1)
                return;
            configId.intValue = newValue;

            var data = FindConfigProperty(configId.intValue);
            if (data != null)
            {
                currentData.FindPropertyRelative(nameof(UIResourceParticle.Config.sprite)).objectReferenceValue = data.FindPropertyRelative(nameof(UIResourceParticle.Config.sprite)).objectReferenceValue;
                currentData.FindPropertyRelative(nameof(UIResourceParticleConfig.Data.life)).floatValue = data.FindPropertyRelative(nameof(UIResourceParticle.Config.life)).floatValue;
                // currentData.FindPropertyRelative(nameof(UIResourceParticle.Config.colorOverLife)).gradientValue = data.FindPropertyRelative(nameof(UIResourceParticle.Config.colorOverLife)).gradientValue; // TODO : It seams unity doesnt have gradient value for serialized Properties
                currentData.FindPropertyRelative(nameof(UIResourceParticle.Config.sizeOverLife)).animationCurveValue = data.FindPropertyRelative(nameof(UIResourceParticle.Config.sizeOverLife)).animationCurveValue;
                currentData.FindPropertyRelative(nameof(UIResourceParticle.Config.rotateOverLife)).animationCurveValue = data.FindPropertyRelative(nameof(UIResourceParticle.Config.rotateOverLife)).animationCurveValue;
                currentData.FindPropertyRelative(nameof(UIResourceParticle.Config.velocityOverLife)).animationCurveValue = data.FindPropertyRelative(nameof(UIResourceParticle.Config.velocityOverLife)).animationCurveValue;
                currentData.FindPropertyRelative(nameof(UIResourceParticle.Config.noiseOverLife)).animationCurveValue = data.FindPropertyRelative(nameof(UIResourceParticle.Config.noiseOverLife)).animationCurveValue;
            }
            else configId.intValue = 0;
        }

        private SerializedProperty FindConfigProperty(int id)
        {
            var config = new SerializedObject(UIResourceParticleConfig.Instance);
            var list = config.FindProperty(nameof(UIResourceParticleConfig.configs));
            foreach (SerializedProperty item in list)
            {
                var itemId = item.FindPropertyRelative(nameof(UIResourceParticleConfig.Data.id));
                if (itemId.intValue == id)
                    return item;
            }
            return null;
        }
    }
}
