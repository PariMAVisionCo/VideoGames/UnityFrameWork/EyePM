﻿#if PM_PARSI
using UnityEditor;

namespace EyePM
{
    [CustomEditor(typeof(LocalizationService))]
    public class LocalizationServiceEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var local = target as LocalizationService;

            if (local.currentKit == null)
            {
                EditorGUILayout.HelpBox("Please select a Kit file for localization service!", MessageType.Warning);
                local.currentKit = (LocalizationKit)EditorGUILayout.ObjectField("Currnet Kit:", local.currentKit, typeof(LocalizationKit), false);
            }
            else base.OnInspectorGUI();
        }
    }

    static class LocalizationMenu
    {
        [MenuItem("EyePM/Localization/Current Kit")]
        private static void CurrentKit()
        {
            Selection.activeObject = LocalizationService.Instance.currentKit;
        }

        [MenuItem("EyePM/Localization/Settings")]
        private static void Settings()
        {
            Selection.activeObject = LocalizationService.Instance;
        }
    }
}
#endif
