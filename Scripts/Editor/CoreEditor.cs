﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace EyePM
{
    [CustomEditor(typeof(Core))]
    public class CoreEditor : Editor
    {
        private class Item
        {
            public string name = string.Empty;
            public string symbol = string.Empty;
            public bool value = false;

            public Item(string name, string symbol) { this.name = name; this.symbol = symbol; }
            public override string ToString() { return name + " " + symbol + " " + value; }
        }


        private string currentSymbols = string.Empty;

        private List<Item> items = new List<Item>(16) {
            new Item("Algorithms", SymbolsList.Algorithms),
            new Item("Camera FX", SymbolsList.CameraFX),
            new Item("Bad words filter", SymbolsList.BadWordsFilter),
            new Item("Console Commands", SymbolsList.ConsoleCommands),
            new Item("Debug Extensions", SymbolsList.DebugExtensions),
            new Item("Mesh Utils", SymbolsList.MeshUtils),
            new Item("Online System", SymbolsList.OnlineSystem),
            new Item("Persian", SymbolsList.Persian),
            new Item("Purchase System", SymbolsList.PurchaseSystem),
            new Item("Screen Joysticks", SymbolsList.ScreenJoysticks),
            new Item("Social Sharing", SymbolsList.SocialSharing),
            new Item("Spline curves", SymbolsList.SplineCurves),
            new Item("Zip Compression", SymbolsList.ZipCompression),
            new Item("Use Old Crypto Key", SymbolsList.UseOldCryptoKey),
        };



        private void OnEnable()
        {
            currentSymbols = SymbolsList.GetCurrentSymbols();
            foreach (var item in items)
                item.value = HasSymbol(currentSymbols, item.symbol);
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Space(30);
            EditorGUILayout.LabelField("Modules:", EditorStyles.boldLabel);

            foreach (var item in items)
                item.value = EditorGUILayout.Toggle(item.name, item.value);

            if (GUILayout.Button("Apply"))
            {
                var resutl = currentSymbols;

                foreach (var item in items)
                    resutl = SymbolsList.AddRemoveSymbol(resutl, item.symbol, item.value);

                if (resutl != currentSymbols)
                    SymbolsList.SetProjectSymbols(resutl);
            }
        }

        private bool HasSymbol(string current, string symbol)
        {
            var cursymbols = new List<string>(current.Split(new char[] { ';' }, System.StringSplitOptions.RemoveEmptyEntries));
            return cursymbols.Contains(symbol);
        }

        [MenuItem("EyePM/Settings", priority = 100)]
        private static void Settings()
        {
            Selection.activeObject = Core.Instance;
        }

        static class EyePMSettings
        {
            private static Editor editor = null;

            [SettingsProvider]
            public static SettingsProvider CreateSettings()
            {
                // First parameter is the path in the Settings window.
                // Second parameter is the scope of this setting: it only appears in the Project Settings window.
                var provider = new SettingsProvider("PariMAVision/EyePM", SettingsScope.Project)
                {
                    // By default the last token of the path is used as display name if no label is provided.
                    label = "EyePM",

                    // Create the SettingsProvider and initialize its drawing (IMGUI) function in place:
                    guiHandler = (searchContext) =>
                    {
                        if (editor == null)
                            editor = CreateEditor(Core.Instance, typeof(CoreEditor));
                        editor.OnInspectorGUI();
                    },

                    // Populate the search keywords to enable smart search filtering and label highlighting:
                    keywords = new HashSet<string>(new[] { "EyePM", "Settings", "Core" })
                };

                return provider;
            }
        }
    }
}
