﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


namespace EyePM
{
    [CustomEditor(typeof(UIShowHide)), CanEditMultipleObjects]
    public class UIShowHideEditor : Editor
    {
        private readonly List<string> popupLabels = new List<string>();
        private readonly List<int> popupIds = new List<int>();

        private void OnEnable()
        {
            popupLabels.Add("None");
            popupIds.Add(0);
            foreach (var item in UIShowHideConfig.Instance.configs)
            {
                popupLabels.Add(item.name);
                popupIds.Add(item.id);
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            var startConfigId = serializedObject.FindProperty(nameof(UIShowHide.startConfigId));
            var hideConfigId = serializedObject.FindProperty(nameof(UIShowHide.hideConfigId));
            var startConfig = serializedObject.FindProperty(nameof(UIShowHide.startConfig));
            var hideConfig = serializedObject.FindProperty(nameof(UIShowHide.hideConfig));

            InspectorConfigPopup("Start Reference", startConfigId, startConfig);
            if (startConfigId.intValue > 0)
            {
                EditorGUILayout.PropertyField(startConfig.FindPropertyRelative(nameof(UIShowHide.Config.duration)), new GUIContent("Start EXTRA Duration"));
                EditorGUILayout.PropertyField(startConfig.FindPropertyRelative(nameof(UIShowHide.Config.delay)), new GUIContent("Start EXTRA Delay"));
            }

            InspectorConfigPopup("Hide Reference", hideConfigId, hideConfig);
            if (hideConfigId.intValue > 0)
            {
                EditorGUILayout.PropertyField(hideConfig.FindPropertyRelative((nameof(UIShowHide.Config.duration))), new GUIContent("Hide EXTRA Duration"));
                EditorGUILayout.PropertyField(hideConfig.FindPropertyRelative((nameof(UIShowHide.Config.delay))), new GUIContent("Hide EXTRA Delay"));
            }

            if (startConfigId.intValue == 0)
                InspectorConfig(startConfig);
            if (hideConfigId.intValue == 0)
                InspectorConfig(hideConfig);

            serializedObject.ApplyModifiedProperties();

            if (EditorApplication.isPlaying == false)
            {
                EditorGUILayout.Space(10);
                EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
                EditorGUILayout.Space(5);

                UIShowHide uiShowHide = (UIShowHide) target;
                if (uiShowHide.gameObject.activeInHierarchy)
                {
                    bool isPlaying = uiShowHide.IsAnyPlaying();
                    GUI.enabled = isPlaying;
                    if (GUILayout.Button("Refactor All If Needed"))
                        uiShowHide.RefactorAllIfNeeded();

                    GUI.enabled = !isPlaying;
                    if (GUILayout.Button("Show All"))
                        uiShowHide.ShowAllEditMode();

                    if (GUILayout.Button("Hide All"))
                        uiShowHide.HideAllEditMode();
                    GUI.enabled = true;

                    EditorGUILayout.Space(5);
                    EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
                    EditorGUILayout.Space(10);
                }
            }
        }

        private void InspectorConfig(SerializedProperty config)
        {
            var rect = EditorGUILayout.GetControlRect(true, 25);
            EditorGUI.HelpBox(rect, string.Empty, MessageType.None);
            config.isExpanded = EditorGUI.Foldout(rect, config.isExpanded, " " + config.displayName, true);
            if (config.isExpanded)
            {
                EditorGUI.indentLevel++;
                config.NextVisible(true);
                EditorGUILayout.PropertyField(config);
                config.NextVisible(false);
                EditorGUILayout.PropertyField(config);
                config.NextVisible(false);
                EditorGUILayout.PropertyField(config);
                config.NextVisible(false);
                EditorGUILayout.PropertyField(config);
                config.NextVisible(false);
                EditorGUILayout.PropertyField(config);
                config.NextVisible(false);
                EditorGUILayout.PropertyField(config);
                config.NextVisible(false);
                EditorGUI.indentLevel--;
            }
        }

        private void InspectorConfigPopup(string label, SerializedProperty configId, SerializedProperty currentState)
        {
            var newValue = EditorGUILayout.IntPopup(label, configId.hasMultipleDifferentValues ? -1 : configId.intValue, popupLabels.ToArray(), popupIds.ToArray());
            if (newValue == -1) return;
            configId.intValue = newValue;

            var state = FindConfigProperty(configId.intValue);
            if (state != null)
            {
                currentState.FindPropertyRelative(nameof(UIShowHideConfig.State.curve)).animationCurveValue = state.FindPropertyRelative(nameof(UIShowHideConfig.State.curve)).animationCurveValue;
                currentState.FindPropertyRelative(nameof(UIShowHideConfig.State.direction)).vector3Value = state.FindPropertyRelative(nameof(UIShowHideConfig.State.direction)).vector3Value;
                currentState.FindPropertyRelative(nameof(UIShowHideConfig.State.scale)).vector3Value = state.FindPropertyRelative(nameof(UIShowHideConfig.State.scale)).vector3Value;
                currentState.FindPropertyRelative(nameof(UIShowHideConfig.State.alpha)).floatValue = state.FindPropertyRelative(nameof(UIShowHideConfig.State.alpha)).floatValue;
            }
            else configId.intValue = 0;
        }

        private SerializedProperty FindConfigProperty(int id)
        {
            var config = new SerializedObject(UIShowHideConfig.Instance);
            var list = config.FindProperty(nameof(UIShowHideConfig.configs));
            foreach (SerializedProperty item in list)
            {
                var itemId = item.FindPropertyRelative(nameof(UIShowHideConfig.State.id));
                if (itemId.intValue == id)
                    return item;
            }
            return null;
        }
    }
}
