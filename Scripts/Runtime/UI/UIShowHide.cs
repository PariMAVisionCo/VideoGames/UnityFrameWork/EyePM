﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;


namespace EyePM
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(CanvasGroup))]
    public class UIShowHide : Base
    {
        private struct State
        {
            public float alpha;
            public Vector3 position;
            public Vector3 scale;
        }

        [System.Serializable]
        public class Config
        {
            public float duration = 0.71f;
            public float delay = 0;
            public AnimationCurve curve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
            public Vector3 direction = Vector3.zero;
            public Vector3 scale = Vector3.one;
            public float alpha = 0;
        }

        public int startConfigId = 0;
        public Config startConfig = new Config();

        public int hideConfigId = 0;
        public Config hideConfig = new Config();

        private float timer = 1;
        private float delayTime = 0;
        private float durationTime = 0;
        private AnimationCurve curve = null;
        private State initiate = new State();
        private State current = new State();
        private State destination = new State();
        private System.Action showhideFunc = null;
        private System.Action endFunc = null;
        private CanvasGroup canvasGroup = null;
        private Animator animator = null;

        #if UNITY_EDITOR
        private float editModePassedTime = 0;
        public bool IsPlaying { private set; get; } = false;
        #endif

        public override bool Visible
        {
            get => canvasGroup.interactable;

            set
            {
                if (value)
                    Show();
                else
                    Hide();
            }
        }

        private void OnEnable()
        {
            All.Add(this);

            #if UNITY_EDITOR
            if (Application.isPlaying == false)
                EditorApplication.update += Update;
            #endif
        }

        private void OnDisable()
        {
            All.Remove(this);

            #if UNITY_EDITOR
            if (Application.isPlaying == false)
            {
                EditorApplication.update -= Update;
            }
            #endif
        }

        private void Awake()
        {
            #if UNITY_EDITOR
            if (Application.isPlaying == false)
                return;
            #endif

            Setup();
            PerformState();

            #if UNITY_EDITOR
            if (Application.isPlaying)
                PerformState();
            #endif
            PerformState();
        }

        private void Setup()
        {
            animator = GetComponent<Animator>();
            canvasGroup = GetComponent<CanvasGroup>();
            #if UNITY_EDITOR
            if (Application.isPlaying)
                canvasGroup.interactable = false;
            #else
            canvasGroup.interactable = false;
            #endif
            endFunc = () =>
            {
                if (animator) animator.enabled = true;
            };

            GetConfigById(startConfig, startConfigId);
            durationTime = GetDuration(startConfig, startConfigId);

            current = initiate = CaptureState();

            destination.position = initiate.position - startConfig.direction * 600;
            destination.scale = startConfig.scale;
            destination.alpha = startConfig.alpha;

            curve = startConfig.curve;
        }

        private void Update()
        {
            #if UNITY_EDITOR
            if (gameObject.activeInHierarchy == false || (Application.isPlaying == false && IsPlaying == false))
                return;
            float editModeDeltaTime = 0;
            if (Application.isPlaying == false)
                editModeDeltaTime = Time.realtimeSinceStartup - editModePassedTime;
            #endif
            if (timer < 1)
            {
                #if UNITY_EDITOR
                if (Application.isPlaying)
                    timer += (Time.unscaledDeltaTime / durationTime);
                else
                    timer += (editModeDeltaTime / durationTime);
                #else
                timer += (Time.unscaledDeltaTime/durationTime);
                #endif
                PerformState();
                if (timer >= 1 && endFunc != null)
                {
                    endFunc();
                    endFunc = null;
                    #if UNITY_EDITOR
                    RefactorToInitialState();
                    #endif
                }
            }

            if (showhideFunc != null)
            {
                #if UNITY_EDITOR
                if (Application.isPlaying)
                    delayTime -= Time.unscaledDeltaTime;
                else
                    delayTime -= editModeDeltaTime;
                #else
                delayTime -= Time.unscaledDeltaTime;
                #endif
                if (delayTime <= 0)
                {
                    showhideFunc();
                    showhideFunc = null;
                }
            }

            #if UNITY_EDITOR
            if (Application.isPlaying == false)
                editModePassedTime = Time.realtimeSinceStartup;
            #endif
        }

        private State CaptureState()
        {
            State res;
            res.position = rectTransform.anchoredPosition;
            res.scale = rectTransform.localScale;
            res.alpha = canvasGroup.alpha;
            return res;
        }

        private void PerformState()
        {
            var t = curve.Evaluate(Mathf.Clamp01(timer));
            rectTransform.anchoredPosition = Vector3.LerpUnclamped(current.position, destination.position, t);
            rectTransform.localScale = Vector3.LerpUnclamped(current.scale, destination.scale, t);
            canvasGroup.alpha = Mathf.Lerp(current.alpha, destination.alpha, t);
        }

        public void Show()
        {
            #if UNITY_EDITOR
            if (Application.isPlaying == false)
                IsPlaying = true;
            #endif
            GetConfigById(startConfig, startConfigId);
            delayTime = GetDelay(startConfig, startConfigId);
            durationTime = GetDuration(startConfig, startConfigId);

            showhideFunc = () =>
            {
                canvasGroup.interactable = true;
                canvasGroup.blocksRaycasts = true;
                current.position = initiate.position - startConfig.direction * 600;
                current.scale = startConfig.scale;
                current.alpha = startConfig.alpha;
                destination = initiate;
                curve = startConfig.curve;
                timer = 0;

                endFunc = () =>
                {
                    if (animator) animator.enabled = true;
                };
            };
        }

        public float Hide()
        {
            #if UNITY_EDITOR
            if (Application.isPlaying == false)
                IsPlaying = true;
            #endif
            GetConfigById(hideConfig, hideConfigId);
            delayTime = GetDelay(hideConfig, hideConfigId);
            durationTime = GetDuration(hideConfig, hideConfigId);

            if (animator) animator.enabled = false;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
            showhideFunc = () =>
            {
                current = CaptureState();
                destination.position = initiate.position + hideConfig.direction * 600;
                destination.scale = hideConfig.scale;
                destination.alpha = hideConfig.alpha;
                curve = hideConfig.curve;
                timer = 0;

                endFunc = () => { };
            };

            return delayTime + durationTime;
        }

        #if UNITY_EDITOR

        public bool IsAnyPlaying()
        {
            return transform.GetComponentsInChildren<UIShowHide>().Any(item => item.IsPlaying);
        }

        private void RefactorToInitialState()
        {
            if(IsPlaying == false)
                return;
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
            destination = initiate;
            timer = 1;
            PerformState();
            if (animator) animator.enabled = true;

            IsPlaying = false;
        }

        public void RefactorAllIfNeeded()
        {
            foreach (var item in transform.GetComponentsInChildren<UIShowHide>())
                item.RefactorToInitialState();
        }

        public void ShowAllEditMode()
        {
            if (IsPlaying || Application.isPlaying || gameObject.activeInHierarchy == false)
                return;
            foreach (var item in transform.GetComponentsInChildren<UIShowHide>())
            {
                item.Setup();
                item.Show();
            }
        }

        public void HideAllEditMode()
        {
            if (IsPlaying || Application.isPlaying || gameObject.activeInHierarchy == false)
                return;
            foreach (var item in transform.GetComponentsInChildren<UIShowHide>())
            {
                item.Setup();
                item.Hide();
            }
        }
        #endif

        ////////////////////////////////////////////////////////
        /// STATIC MEMBER
        ////////////////////////////////////////////////////////
        private static readonly List<UIShowHide> All = new List<UIShowHide>();

        public static void ShowAll(Transform parent)
        {
            foreach (var item in All)
                if (item.transform.IsChildOf(parent))
                    item.Show();
        }

        public static float HideAll(Transform parent)
        {
            float res = 0;
            foreach (var item in All)
                if (item.transform.IsChildOf(parent))
                {
                    var hideTime = item.Hide();
                    if (hideTime > res)
                        res = hideTime;
                }
            return res;
        }

        // copy preset config to dest and return new delay time
        private static void GetConfigById(Config dest, int id)
        {
            var config = UIShowHideConfig.GetStateById(id);
            if (config == null)
                return;
            dest.curve = config.curve;
            dest.direction = config.direction;
            dest.scale = config.scale;
            dest.alpha = config.alpha;
        }

        private static float GetDelay(Config dest, int id)
        {
            var config = UIShowHideConfig.GetStateById(id);
            if (config == null)
                return dest.delay;
            return dest.delay + config.delay;
        }

        private static float GetDuration(Config dest, int id)
        {
            var config = UIShowHideConfig.GetStateById(id);
            if (config == null)
                return dest.duration;
            return dest.duration + config.duration;
        }
    }
}
