﻿using UnityEngine;


namespace EyePM
{
    public class Core : ScriptableObject
    {
        [System.Serializable]
        public class OnlineOptions
        {
            public int gameId = 0;
            public string onlineDomain = "EyePM.Com";
        }

        [System.Serializable]
        public class SecurityOptions
        {
            public string cryptoKey = "Replace crypto key here";
            public string salt = "RasGames";
        }

        #if UNITY_EDITOR
        [System.Serializable]
        public class TestDevices
        {
            public bool active = false;
            public int index = 0;
            [Header("List of devices for test"), Space]
            public string[] list = new string[0];
        }
        #endif

        [SerializeField] private SecurityOptions securityOptions = new SecurityOptions();
        [Space]
        [SerializeField] private OnlineOptions onlineOptions = new OnlineOptions();

        #if UNITY_EDITOR
        [Space]
        [SerializeField] private TestDevices testDevices = new TestDevices();
        #endif


        private string baseDeviceId = string.Empty;
        private string deviceId = string.Empty;
        private string hashSalt = string.Empty;
        private byte[] cryptoKey = null;

        protected void OnInitialize()
        {
            #if UNITY_EDITOR
            baseDeviceId = deviceId = testDevices.active ? testDevices.list[testDevices.index] : SystemInfo.deviceUniqueIdentifier;
            #else
            baseDeviceId = SystemInfo.deviceUniqueIdentifier;
            deviceId = ComputeMD5(baseDeviceId, securityOptions.salt);
            #endif
            hashSalt = ComputeMD5(securityOptions.salt, securityOptions.salt);
            #if PM_OLDCORE
            cryptoKey = System.Text.Encoding.ASCII.GetBytes(securityOptions.cryptokey);
            #else
            cryptoKey = System.Text.Encoding.ASCII.GetBytes(securityOptions.cryptoKey + hashSalt);
            #endif

            #if UNITY_EDITOR
            #else
            securityOptions.cryptoKey = string.Empty;
            securityOptions.salt = string.Empty;
            #endif
        }

        ////////////////////////////////////////////////////////////
        /// STATIC MEMBERS
        ////////////////////////////////////////////////////////////
        public static int GameId => Instance.onlineOptions.gameId;
        public static string OnlineDomain => Instance.onlineOptions.onlineDomain;
        public static string BaseDeviceId => Instance.baseDeviceId;
        public static string DeviceId => Instance.deviceId;
        public static string Salt => Instance.hashSalt;
        public static byte[] CryptoKey => Instance.cryptoKey;

        private static Core instance = default;

        public static Core Instance
        {
            get
            {
                #if UNITY_EDITOR
                if (instance == null) CreateMe(CreateInstance<Core>(), nameof(Core));
                #endif
                if (instance == null)
                {
                    instance = Resources.Load<Core>("BaseConfigs/" + nameof(Core));
                    instance.OnInitialize();
                }
                return instance;
            }
        }

        public static string ComputeMD5(string str, string salt)
        {
            var md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(str + salt);
            byte[] hashBytes = md5.ComputeHash(inputBytes);
            var res = new System.Text.StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
                res.Append(hashBytes[i].ToString("X2"));
            return res.ToString();
        }

        public static byte[] Encrypt(byte[] data)
        {
            var res = new byte[data.Length];
            for (int i = 0; i < data.Length; ++i)
                res[i] = (byte) (data[i] + CryptoKey[i % CryptoKey.Length]);
            return res;
        }

        public static byte[] Decrypt(byte[] data)
        {
            var res = new byte[data.Length];
            for (int i = 0; i < data.Length; ++i)
                res[i] = (byte) (data[i] - CryptoKey[i % CryptoKey.Length]);
            return res;
        }


        #if UNITY_EDITOR
        private static void CreateMe(Object instance, string name)
        {
            var path ="/_" + Application.productName.Replace(" ", string.Empty) + "/Resources/BaseConfigs/";
            var appPath = Application.dataPath + path;
            var fileName = path + name + ".asset";
            if (System.IO.File.Exists(Application.dataPath + fileName)) return;
            if (!System.IO.Directory.Exists(appPath)) System.IO.Directory.CreateDirectory(appPath);
            UnityEditor.AssetDatabase.CreateAsset(instance, "Assets" + fileName);
            UnityEditor.AssetDatabase.SaveAssets();
        }
        #endif
    }
}
