﻿using UnityEngine.Scripting;

namespace EyePM
{
    public interface IMessages
    {
        [Preserve]
        void OnMessage(Messages.Param param);
    }
}