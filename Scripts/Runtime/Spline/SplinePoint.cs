﻿#if PM_SPLINE
using UnityEngine;

namespace EyePM.Spline
{
    public class SplinePoint : MonoBehaviour
    {
        public bool autoPower = true;
        public float power = 1;
    }
}
#endif
