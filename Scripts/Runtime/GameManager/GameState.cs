﻿namespace EyePM
{
    public abstract class GameState : Base
    {
        //  Will be called before destroying this object. return time of closing animation
        public virtual float PreClose()
        {
            return UIShowHide.HideAll(transform);
        }

        //  will be called whene back button pressed
        public virtual void Back()
        {
            GameManager.Game.Back(this);
        }

        public virtual void Reset()
        {
            gameObject.name = GetType().Name;
        }
    }

}
