﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using Unity.EditorCoroutines.Editor;
#endif

using UnityEngine;

namespace EyePM
{
    public interface IUIResourceManagerBase
    {
        void Add(int amount, RectTransform startRectTransform);
        void Add(int amount, Transform startTransform, Canvas canvas);
    }

    public interface IUIResourceManager
    {

    }

    [ExecuteInEditMode]
    [RequireComponent(typeof(UIResourceParticle))]
    public class UIResourceParticleManager<T> : MonoBehaviour, IUIResourceManagerBase where T : IUIResourceManager
    {
        [SerializeField] private float startPointRadius = 0.32f;
        [SerializeField] private Vector2 intervalMinMax = new Vector2(0.05f, 0.1f);

        private UIResourceParticle uiResourceParticle = null;
        private RectTransform rectTransform = null;

        public UIResourceParticle UIResourceParticle
        {
            get
            {
                if (uiResourceParticle == null)
                    uiResourceParticle = GetComponent<UIResourceParticle>();
                return uiResourceParticle;
            }
        }

        public RectTransform RectTransform
        {
            get
            {
                if (rectTransform == null)
                    rectTransform = GetComponent<RectTransform>();
                return rectTransform;
            }
        }

        public void Add(int amount, RectTransform startRectTransform)
        {
            #if UNITY_EDITOR
            if (Application.isPlaying)
                StartCoroutine(DoAdd(amount, startRectTransform));
            else
                EditorCoroutineUtility.StartCoroutine(DoAdd(amount, startRectTransform), this);
            #else
            StartCoroutine(DoAdd(amount, startRectTransform));
            #endif
        }

        public void Add(int amount, Transform startTransform, Canvas canvas)
        {
            #if UNITY_EDITOR
            if (Application.isPlaying)
                StartCoroutine(DoAdd(amount, startTransform, canvas));
            else
                EditorCoroutineUtility.StartCoroutine(DoAdd(amount, startTransform, canvas), this);
            #else
            StartCoroutine(DoAdd(amount, startTransform, canvas));
            #endif
        }

        private IEnumerator DoAdd(int amount, RectTransform startRectTransform)
        {
            for (int i = 0; i < amount; i++)
            {
                UIResourceParticle.Add(RectTransform.LocalPointToLocalPointInRectangle(startRectTransform, null) * -1 + Random.insideUnitCircle * startPointRadius);
                yield return new WaitForSecondsRealtime(Random.Range(intervalMinMax.x, intervalMinMax.y));
            }
        }

        private IEnumerator DoAdd(int amount, Transform startTransform, Canvas canvas)
        {
            for (int i = 0; i < amount; i++)
            {
                Vector2 anchoredPosition = RectTransform.WorldPointToLocalPoint(startTransform.position, null, canvas);

                UIResourceParticle.Add(anchoredPosition + Random.insideUnitCircle * startPointRadius);
                yield return new WaitForSecondsRealtime(Random.Range(intervalMinMax.x, intervalMinMax.y));
            }
        }

        private void OnEnable()
        {
            All.Add(this);
        }

        private void OnDisable()
        {
            All.Remove(this);
        }

        //////////////////////////////////////////////////////
        /// STATIC MEMBERS
        //////////////////////////////////////////////////////
        private static readonly List<UIResourceParticleManager<T>> All = new List<UIResourceParticleManager<T>>();

        public static void AddResource(int amount, RectTransform startRectTransform)
        {
            foreach (var item in All)
                item.Add(amount, startRectTransform);
        }

        public static void AddResource(int amount, Transform startTransform, Canvas canvas)
        {
            foreach (var item in All)
                item.Add(amount, startTransform, canvas);
        }
    }
}
