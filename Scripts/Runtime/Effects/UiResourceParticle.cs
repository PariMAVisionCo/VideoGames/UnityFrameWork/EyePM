﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;


namespace EyePM
{
    [ExecuteInEditMode]
    [AddComponentMenu("UI/EyePM/ResourceParticle", 20), ExecuteAlways]
    public class UIResourceParticle : MaskableGraphic
    {
        private struct Quad
        {
            public float age;
            public float noise;
            public Vector3 start;
            public Vector3 right;
            public Vector3 position;
        }

        [Serializable]
        public class Config
        {
            public Sprite sprite = null;
            public float life = 2;
            public Gradient colorOverLife = new Gradient();
            public AnimationCurve sizeOverLife = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
            public AnimationCurve rotateOverLife = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 0));
            public AnimationCurve velocityOverLife = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
            public AnimationCurve noiseOverLife = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 0));
        }

        public int configId = 0;
        public Config config = new Config();

        private readonly List<Quad> quads = new List<Quad>(64);
        #if UNITY_EDITOR
        private float editModePassedTime = 0;
        #endif

        public override Texture mainTexture => config.sprite ? config.sprite.texture : base.mainTexture;

        #if UNITY_EDITOR
        private void OnPopulateMesh()
        {
            if(Application.isPlaying)
                return;
            Mesh mesh = new Mesh();
            mesh.vertices = Array.ConvertAll(config.sprite.vertices, i => (Vector3) i);
            mesh.uv = config.sprite.uv;
            mesh.triangles = Array.ConvertAll(config.sprite.triangles, i => (int) i);

            OnPopulateMesh(new VertexHelper(mesh));
        }
        #endif

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            vh.Clear();

            var uv = config.sprite ? UnityEngine.Sprites.DataUtility.GetOuterUV(config.sprite) : Vector4.zero;
            var sw = rectTransform.rect.width * 0.5f;
            var sh = rectTransform.rect.height * 0.5f;

            var quad = new UIVertex[4];

            foreach (Quad current in quads)
            {
                if (current.age < 0 || current.age > 1) continue;

                var qRotate = config.rotateOverLife.Evaluate(current.age) * current.noise;
                var qColor = config.colorOverLife.Evaluate(current.age) * color;
                var qSize = config.sizeOverLife.Evaluate(current.age);
                var qw = sw * qSize;
                var qh = sh * qSize;
                var sinR = Mathf.Sin(qRotate);
                var cosR = Mathf.Cos(qRotate);
                var xSin = qw * sinR;
                var xCos = qw * cosR;
                var ySin = qh * sinR;
                var yCos = qh * cosR;

                quad[0] = UIVertex.simpleVert;
                quad[0].color = qColor;
                quad[0].position = new Vector3(-xCos + ySin, -xSin - yCos, 0) + current.position;
                quad[0].uv0 = new Vector2(uv.x, uv.y);

                quad[1] = UIVertex.simpleVert;
                quad[1].color = qColor;
                quad[1].position = new Vector3(-xCos - ySin, -xSin + yCos, 0) + current.position;
                quad[1].uv0 = new Vector2(uv.x, uv.w);

                quad[2] = UIVertex.simpleVert;
                quad[2].color = qColor;
                quad[2].position = new Vector3(xCos - ySin, xSin + yCos, 0) + current.position;
                quad[2].uv0 = new Vector2(uv.z, uv.w);

                quad[3] = UIVertex.simpleVert;
                quad[3].color = qColor;
                quad[3].position = new Vector3(xCos + ySin, xSin - yCos, 0) + current.position;
                quad[3].uv0 = new Vector2(uv.z, uv.y);

                vh.AddUIVertexQuad(quad);
            }

            #if UNITY_EDITOR
            if (Application.isPlaying == false && quads.Count < 1)
            {
                quad[0] = UIVertex.simpleVert;
                quad[0].color = color;
                quad[0].position = new Vector3(-sw, -sh, 0);
                quad[0].uv0 = new Vector2(uv.x, uv.y);

                quad[1] = UIVertex.simpleVert;
                quad[1].color = color;
                quad[1].position = new Vector3(-sw, sh, 0);
                quad[1].uv0 = new Vector2(uv.x, uv.w);

                quad[2] = UIVertex.simpleVert;
                quad[2].color = color;
                quad[2].position = new Vector3(sw, sh, 0);
                quad[2].uv0 = new Vector2(uv.z, uv.w);

                quad[3] = UIVertex.simpleVert;
                quad[3].color = color;
                quad[3].position = new Vector3(sw, -sh, 0);
                quad[3].uv0 = new Vector2(uv.z, uv.y);

                vh.AddUIVertexQuad(quad);
            }
            #endif
        }

        #if UNITY_EDITOR
        private new void OnEnable()
        {
            if (Application.isPlaying == false)
            {
                EditorApplication.update += Update;
                EditorApplication.update += OnPopulateMesh;
            }
        }

        private new void OnDisable()
        {
            if (Application.isPlaying == false)
            {
                EditorApplication.update -= Update;
                EditorApplication.update -= OnPopulateMesh;
            }
        }
        #endif

        private void Update()
        {
            #if UNITY_EDITOR
            float editModeDeltaTime = 0;
            if (Application.isPlaying == false)
                editModeDeltaTime = Time.realtimeSinceStartup - editModePassedTime;
            #endif

            quads.RemoveAll(x => x.age > 1);

            bool isDirty = false;
            for (int i = 0; i < quads.Count; i++)
            {
                var quad = quads[i];
                if (quad.age > 1) continue;

                #if UNITY_EDITOR
                if (Application.isPlaying)
                    quad.age += Time.unscaledDeltaTime / config.life;
                else
                    quad.age += editModeDeltaTime / config.life;
                #else
                quad.age += Time.unscaledDeltaTime / config.life;
                #endif

                var time = config.velocityOverLife.Evaluate(quad.age);
                quad.position = Vector2.Lerp(quad.start, Vector3.zero, time);
                quad.position += quad.right * (quad.noise * config.noiseOverLife.Evaluate(time) * 100);

                quads[i] = quad;
                isDirty = true;
            }


            #if UNITY_EDITOR
            if (Application.isPlaying == false)
                editModePassedTime = Time.realtimeSinceStartup;
            #endif

            if (isDirty)
                SetAllDirty();
        }


        public void Add(Vector2 startPoint)
        {
            quads.Add(new Quad()
                      {
                          age = 0,
                          noise = Random.value * 2 - 1,
                          start = startPoint,
                          right = Vector3.Cross(Vector3.forward, startPoint).normalized,
                          position = startPoint
                      });
        }
    }
}
