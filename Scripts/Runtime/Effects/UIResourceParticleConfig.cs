﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace EyePM
{
    public class UIResourceParticleConfig : StaticConfig<UIResourceParticleConfig>
    {
        [Serializable]
        public class Data
        {
            public string name = string.Empty;
            public Sprite sprite = null;
            public float life = 2;
            public Gradient colorOverLife = new Gradient();
            public AnimationCurve sizeOverLife = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
            public AnimationCurve rotateOverLife = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 0));
            public AnimationCurve velocityOverLife = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
            public AnimationCurve noiseOverLife = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 0));
            public int id = 0;
        }

        public List<Data> configs = new List<Data>();

        private void OnValidate()
        {
            int maxId = 0;
            foreach (var item in configs)
            {
                if (item.id > maxId)
                    maxId = item.id;
            }

            foreach (var item in configs)
            {
                if (item.id < 1)
                    item.id = ++maxId;
            }
        }

        //////////////////////////////////////////////////////
        /// STATIC MEMBERS
        //////////////////////////////////////////////////////
        public static Data GetDataById(int id, Data defaultState = null)
        {
            var res = id > 0 ? Instance.configs.Find(x => x.id == id) : defaultState;
            return res ?? defaultState;
        }
    }
}
