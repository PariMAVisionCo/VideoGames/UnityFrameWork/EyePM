﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEditor;


namespace EyePM
{
    public static class SymbolsList
    {
        public const string Algorithms = "PM_ALGOR";
        public const string CameraFX = "PM_CAMFX";
        public const string BadWordsFilter = "PM_CENSOR";
        public const string ConsoleCommands = "PM_CCMD";
        public const string DebugExtensions = "PM_EXDBG";
        public const string MeshUtils = "PM_MESH";
        public const string OnlineSystem = "PM_ONLINE";
        public const string Persian = "PM_PARSI";
        public const string PurchaseSystem = "PM_IAP";
        public const string ScreenJoysticks = "PM_SCRJOY";
        public const string SocialSharing = "PM_SHRNG";
        public const string SplineCurves = "PM_SPLINE";
        public const string ZipCompression = "PM_ZIP";
        public const string UseOldCryptoKey = "PM_OLDCORE";
        public const string ShaderOop = "PM_OOP";
        public const string ShaderSimple = "PM_SIMPLE";

        public const string TvBuild = "TV";
        public const string GoogleBuild = "GOOGLE";
        public const string BazaarBuild = "BAZAAR";
        public const string MyKetBuild = "MYKET";

        public const string GoogleAnalythics = "GASDK";
        public const string METRIX = "METRIX";
        public const string Tapsell = "TAPSELL";
        public const string TapsellPlus = "TAPSELLPlus";
        public const string AdMob = "ADMOB";
        public const string UAD = "UAD";

        public const string DebugMode = "DEBUG_MODE";

        private static BuildTargetGroup CurrentTargetGroup
        {
            get
            {
                #if UNITY_ANDROID
                    return BuildTargetGroup.Android;
                #elif UNITY_IOS
                    return UnityEditor.BuildTargetGroup.iOS;
                #elif UNITY_STANDALONE
                return UnityEditor.BuildTargetGroup.Standalone;
                #elif UNITY_WEBGL
                    return UnityEditor.BuildTargetGroup.WebGL;
                #endif
            }
        }

        public static string AddRemoveSymbol(string current, string symbol, bool addSymbols)
        {
            if (string.IsNullOrEmpty(symbol)) return current;

            var curSymbols = new List<string>(current.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries));
            var newSymbols = new List<string>(symbol.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries));

            //  remove duplicated symbols
            foreach (var item in newSymbols)
                curSymbols.RemoveAll(x => x == item);

            //  add new symbols
            if (addSymbols)
                curSymbols.AddRange(newSymbols);

            curSymbols.Sort();
            return string.Join(";", curSymbols.ToArray());
        }

        public static void SetProjectSymbols(string value)
        {
            SetProjectSymbols(CurrentTargetGroup, value);
        }

        public static void SetProjectSymbols(BuildTargetGroup buildTargetGroup, string value)
        {
            PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, value);
        }

        public static string GetCurrentSymbols()
        {
            return GetCurrentSymbols(CurrentTargetGroup);
        }

        public static string GetCurrentSymbols(BuildTargetGroup buildTargetGroup)
        {
            return PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup);
        }

    }
}
#endif